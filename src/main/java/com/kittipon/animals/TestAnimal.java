/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipon.animals;

/**
 *
 * @author kitti
 */
public class TestAnimal {

    public static void main(String[] args) {
        Human h1 = new Human("Nat");
        h1.run();
        h1.eat();
        h1.speak();
        h1.sleep();
        nextLine();
        Cat c1 = new Cat("Somsri");
        c1.run();
        c1.eat();
        c1.speak();
        c1.sleep();
        nextLine();
        Dog d1 = new Dog("Somsak");
        d1.run();
        d1.eat();
        d1.speak();
        d1.sleep();
        nextLine();
        Crocodiles cd1 = new Crocodiles("Kraithong");
        cd1.Crawl();
        cd1.eat();
        cd1.speak();
        cd1.sleep();
        nextLine();
        Snake s1 = new Snake("SiSri");
        s1.Crawl();
        s1.eat();
        s1.speak();
        s1.sleep();
        nextLine();
        Fish f1 = new Fish("Yut");
        f1.swim();
        f1.eat();
        f1.speak();
        f1.sleep();
        nextLine();
        Crab cb1 = new Crab("Pu");
        cb1.swim();
        cb1.eat();
        cb1.speak();
        cb1.sleep();
        nextLine();
        Bat b1 = new Bat("Covid");
        b1.fly();
        b1.eat();
        b1.speak();
        b1.sleep();
        nextLine();
        Bird bd1 = new Bird("Jeab");
        bd1.fly();
        bd1.eat();
        bd1.speak();
        bd1.sleep();
        nextLine();
 //-------------------------------------
        Bat bat = new Bat("Baat");
        Plane plane = new Plane("Engine number 1");
        bat.fly();
        plane.fly();
        Dog dog = new Dog("doogie");
        Flyable[] flyable = {bat, plane};
        for(Flyable f:flyable){
            if(f instanceof Plane){
                Plane p = (Plane)f;
                p.startEngine();
                p.raiseSpeed();
                p.run();
                System.out.println("plane : take off");
                p.fly();
                p.raiseSpeed();
                p.raiseSpeed();
                p.raiseSpeed();
                p.applyBreak();
                System.out.println("plane : take down");
                p.applyBreak();
                break;
            }
            f.fly();
        }
        Runable[] runables = {dog,plane};
        for(Runable r: runables){
            if(r instanceof Dog){
                Dog d = (Dog) r;
                d.bark();
                d.bark();
                d.bark();
                break;
            }
            r.run();
        }
    }

    private static void nextLine() {
        System.out.println("");
    }
}
